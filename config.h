/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
	[BACKGROUND] = "#1c1f26", /* after initialization */
	[INIT]       = "#1c1f26", /* after initialization */
	[INPUT]      = "#668cb3", /* during input */
	[FAILED]     = "#bf4551", /* wrong password */
	[CAPS]       = "#ebb64b", /* capslock on */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* insert grid pattern with scale 1:1, the size can be changed with logosize */
static const int logosize = 25;
static const int logow = 8;	/* grid width and height for right center alignment*/
static const int logoh = 7;

static XRectangle rectangles[] = {
	/* x	y	w	h */
	{ 2,	0,	4,	1 },
	{ 1,	1,	1,	1 },
	{ 3,	1,	2,	1 },
	{ 6,	1,	1,	1 },
	{ 0,	2,	8,	1 },
	{ 0,	3,	1,	3 },
	{ 7,	3,	1,	3 },
	{ 2,	3,	1,	4 },
	{ 5,	3,	1,	4 },
};
